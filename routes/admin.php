<?php
Route::get('/', 'AdminController@index')->name('dashboard');
Route::get('/home', 'AdminController@home')->name('home');
Route::put('/home', 'AdminController@update')->name('home.update');
