<?php

/**
 * Front End Routes
 */
Route::get('/', 'HomeController@index')->name('front.home');

/**
 * Admin Routes
 */
Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
], function () {
    require_once 'admin.php';
});

