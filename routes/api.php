<?php
Route::get('/home', 'API\\HomeController@index');
Route::put('/home', 'API\\HomeController@store');
Route::get('/home/{home}', 'API\\HomeController@show');
