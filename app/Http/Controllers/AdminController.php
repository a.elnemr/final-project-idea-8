<?php

namespace App\Http\Controllers;

use App\Home;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.dashboard');
    }

    public function home()
    {
        return view('admin.home', [
            'home' => Home::first()
        ]);
    }

    public function update(Request $request)
    {
        $home = Home::first();
        $home->update($request->all());
        return back();
    }
}
