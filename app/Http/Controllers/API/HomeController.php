<?php

namespace App\Http\Controllers\API;

use App\Home;
use App\Http\Controllers\Controller;
use App\Http\Requests\HomeRequest;
use App\Http\Resources\HomeList;
use App\Http\Resources\HomeShow;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return HomeList::collection(Home::all());
    }

    public function show(Home $home)
    {
        return new HomeShow($home);
    }

    public function store(HomeRequest $request)
    {
        $home = Home::first();
        $home->update($request->all());
        return new HomeShow($home);
    }
}
