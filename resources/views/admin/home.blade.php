@extends('adminlte::page')

@section('title', 'Home')

@section('content_header')
    <h1>Home</h1>
@stop

@section('content')
    <div class="card p-5">
        <form action="{{ route('admin.home.update') }}" method="post">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="phone">Phone</label>
                <input type="text"
                       id="phone"
                       name="phone"
                       value="{{ $home->phone }}"
                       class="form-control">
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="email"
                       id="email"
                       name="email"
                       value="{{ $home->email }}"
                       class="form-control">
            </div>
            <div class="form-group">
                <label for="address">Address</label>
                <input type="text"
                       id="address"
                       name="address"
                       value="{{ $home->address }}"
                       class="form-control">
            </div>

            <div class="text-center">
                <button class="btn btn-success" type="submit">Save</button>
            </div>
        </form>
    </div>
@stop
