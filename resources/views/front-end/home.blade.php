<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->


<!-- Mirrored from webdesign-finder.com/html/qworks/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 Jan 2020 15:30:01 GMT -->
<head>
    <title>qWorx - HTML template</title>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="/front-end/css/bootstrap.min.css">
    <link rel="stylesheet" href="/front-end/css/animations.css">
    <link rel="stylesheet" href="/front-end/css/font-awesome.css">
    <link rel="stylesheet" href="/front-end/css/main.css" class="color-switcher-link">
    <script src="/front-end/js/vendor/modernizr-custom.js"></script>

    <!--[if lt IE 9]>
    <script src="/front-end/js/vendor/html5shiv.min.js"></script>
    <script src="/front-end/js/vendor/respond.min.js"></script>
    <script src="/front-end/js/vendor/jquery-1.12.4.min.js"></script>
    <![endif]-->

</head>

<body>
<!--[if lt IE 9]>
<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a
    href="http://browsehappy.com/" class="color-main">upgrade your browser</a> to improve your experience.
</div>
<![endif]-->

<div class="preloader">
    <div class="preloader_image"></div>
</div>

<!-- search modal -->
<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="widget widget_search">
        <form method="get" class="searchform search-form" action="http://webdesign-finder.com/">
            <div class="form-group">
                <input type="text" value="" name="search" class="form-control" placeholder="Search keyword"
                       id="modal-search-input">
            </div>
            <button type="submit" class="btn">Search</button>
        </form>
    </div>
</div>

<!-- Unyson messages modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
    <div class="fw-messages-wrap ls p-normal">
        <!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
        <!--
    <ul class="list-unstyled">
        <li>Message To User</li>
    </ul>
    -->

    </div>
</div><!-- eof .modal -->

<!-- wrappers for visual page editor and boxed version of template -->
<div id="canvas">
    <div id="box_wrapper">

        <!-- template sections -->

        <!--topline section visible only on small screens|-->
        <section class="page_topline ls c-my-20 s-borderbottom">
            <div class="container-fluid">
                <div class="row align-items-center d-flex justify-content-md-end">

                    <div class="col-md-8 top_meta text-center">
                        <ul class="top-includes">

                            <li>
									<span class="icon-inline">
										<span class="icon-styled color-main">
											<i class="fa fa-pencil color-main3"></i>
										</span>
										<span>
											{{ $home->email }}
										</span>
									</span>
                            </li>
                            <li>
									<span class="icon-inline">
										<span class="icon-styled color-main">
											<i class="fa fa-map-marker color-main2"></i>
										</span>
										<span>
											{{ $home->address }}
										</span>
									</span>
                            </li>
                            <li>
									<span class="icon-inline">
										<span class="icon-styled color-main">
											<i class="fa fa-phone"></i>
										</span>
										<span>
											{{ $home->phone }}
										</span>
									</span>
                            </li>

                        </ul>
                    </div>

                    <div class="col-sm-12 col-md-2 text-md-right text-center">

							<span class="icon-inline">
								<span class="icon-styled color-main">
									<i class="fa fa-user color-main4"></i>
								</span>
								<span>
									Join Us / Log in
								</span>
							</span>

                    </div>
                </div>
            </div>
        </section>
        <!--eof topline-->

        <!-- header with three Bootstrap columns - left for logo, center for navigation and right for includes-->
        <header class="page_header ds mainheader">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-xl-2 col-lg-3 col-11">
                        <a href="index.html" class="logo">
                            <img src="/front-end/images/logo.png" alt="logo">
                        </a>
                    </div>
                    <div class="col-xl-8 col-lg-5 col-1 text-sm-center">
                        <!-- main nav start -->
                        <nav class="top-nav">
                            <ul class="nav sf-menu">


                                <li class="active">
                                    <a href="index-2.html">Home</a>
                                    <ul>
                                        <li>
                                            <a href="index-2.html">MultiPage</a>
                                        </li>
                                        <li>
                                            <a href="index_static.html">Static Intro</a>
                                        </li>
                                        <li>
                                            <a href="index_singlepage.html">Single Page</a>
                                        </li>
                                    </ul>
                                </li>

                                <!-- blog -->
                                <li>
                                    <a href="blog-right.html">Blog</a>
                                    <ul>

                                        <li>
                                            <a href="blog-right.html">Right Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="blog-left.html">Left Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="blog-full.html">No Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="blog-grid.html">Blog Grid</a>
                                        </li>

                                        <li>
                                            <a href="blog-single-right.html">Post</a>
                                            <ul>
                                                <li>
                                                    <a href="blog-single-right.html">Right Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="blog-single-left.html">Left Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="blog-single-full.html">No Sidebar</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a href="blog-single-video-right.html">Video Post</a>
                                            <ul>
                                                <li>
                                                    <a href="blog-single-video-right.html">Right Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="blog-single-video-left.html">Left Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="blog-single-video-full.html">No Sidebar</a>
                                                </li>
                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <!-- eof blog -->

                                <li>
                                    <a href="#">Features</a>
                                    <div class="mega-menu">
                                        <ul class="mega-menu-row">
                                            <li class="mega-menu-col">
                                                <a href="#">Headers</a>
                                                <ul>
                                                    <li>
                                                        <a href="header1.html">Header Type 1</a>
                                                    </li>
                                                    <li>
                                                        <a href="header2.html">Header Type 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="header3.html">Header Type 3</a>
                                                    </li>
                                                    <li>
                                                        <a href="header4.html">Header Type 4</a>
                                                    </li>
                                                    <li>
                                                        <a href="header5.html">Header Type 5</a>
                                                    </li>
                                                    <li>
                                                        <a href="header6.html">Header Type 6</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="mega-menu-col">
                                                <a href="#">Side Menus</a>
                                                <ul>
                                                    <li>
                                                        <a href="header-side.html">Push Left</a>
                                                    </li>
                                                    <li>
                                                        <a href="header-side-right.html">Push Right</a>
                                                    </li>
                                                    <li>
                                                        <a href="header-side-slide.html">Slide Left</a>
                                                    </li>
                                                    <li>
                                                        <a href="header-side-slide-right.html">Slide Right</a>
                                                    </li>
                                                    <li>
                                                        <a href="header-side-sticked.html">Sticked Left</a>
                                                    </li>
                                                    <li>
                                                        <a href="header-side-sticked-right.html">Sticked Right</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="mega-menu-col">
                                                <a href="title1.html">Title Sections</a>
                                                <ul>
                                                    <li>
                                                        <a href="title1.html">Title section 1</a>
                                                    </li>
                                                    <li>
                                                        <a href="title2.html">Title section 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="title3.html">Title section 3</a>
                                                    </li>
                                                    <li>
                                                        <a href="title4.html">Title section 4</a>
                                                    </li>
                                                    <li>
                                                        <a href="title5.html">Title section 5</a>
                                                    </li>
                                                    <li>
                                                        <a href="title6.html">Title section 6</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="mega-menu-col">
                                                <a href="footer1.html#footer">Footers</a>
                                                <ul>
                                                    <li>
                                                        <a href="footer1.html#footer">Footer Type 1</a>
                                                    </li>
                                                    <li>
                                                        <a href="footer2.html#footer">Footer Type 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="footer3.html#footer">Footer Type 3</a>
                                                    </li>
                                                    <li>
                                                        <a href="footer4.html#footer">Footer Type 4</a>
                                                    </li>
                                                    <li>
                                                        <a href="footer5.html#footer">Footer Type 5</a>
                                                    </li>
                                                    <li>
                                                        <a href="footer6.html#footer">Footer Type 6</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="mega-menu-col">
                                                <a href="copyright1.html#copyright">Copyright</a>

                                                <ul>
                                                    <li>
                                                        <a href="copyright1.html#copyright">Copyright 1</a>
                                                    </li>
                                                    <li>
                                                        <a href="copyright2.html#copyright">Copyright 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="copyright3.html#copyright">Copyright 3</a>
                                                    </li>
                                                    <li>
                                                        <a href="copyright4.html#copyright">Copyright 4</a>
                                                    </li>
                                                    <li>
                                                        <a href="copyright5.html#copyright">Copyright 5</a>
                                                    </li>
                                                    <li>
                                                        <a href="copyright6.html#copyright">Copyright 6</a>
                                                    </li>
                                                </ul>
                                            </li>

                                        </ul>
                                    </div> <!-- eof mega menu -->
                                </li>
                                <!-- eof features -->


                                <li>
                                    <a href="about.html">Pages</a>
                                    <ul>


                                        <li>
                                            <a href="about.html">About</a>
                                        </li>

                                        <li>
                                            <a href="services.html">Our Services</a>
                                            <ul>
                                                <li>
                                                    <a href="services.html">Services 1</a>
                                                </li>
                                                <li>
                                                    <a href="services2.html">Services 2</a>
                                                </li>
                                                <li>
                                                    <a href="services3.html">Services 3</a>
                                                </li>
                                                <li>
                                                    <a href="service-single.html">Single Service</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a href="pricing.html">Pricing</a>
                                        </li>

                                        <!-- shop -->
                                        <li>
                                            <a href="shop-right.html">Shop</a>
                                            <ul>
                                                <li>
                                                    <a href="shop-account-dashboard.html">Account</a>
                                                    <ul>

                                                        <li>
                                                            <a href="shop-account-details.html">Account details</a>
                                                        </li>
                                                        <li>
                                                            <a href="shop-account-addresses.html">Addresses</a>
                                                        </li>
                                                        <li>
                                                            <a href="shop-account-address-edit.html">Edit Address</a>
                                                        </li>
                                                        <li>
                                                            <a href="shop-account-orders.html">Orders</a>
                                                        </li>
                                                        <li>
                                                            <a href="shop-account-order-single.html">Single Order</a>
                                                        </li>
                                                        <li>
                                                            <a href="shop-account-downloads.html">Downloads</a>
                                                        </li>
                                                        <li>
                                                            <a href="shop-account-password-reset.html">Password
                                                                Reset</a>
                                                        </li>
                                                        <li>
                                                            <a href="shop-account-login.html">Login/Logout</a>
                                                        </li>

                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="shop-right.html">Right Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="shop-left.html">Left Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="shop-product-right.html">Product Right Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="shop-product-left.html">Product Left Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="shop-cart.html">Cart</a>
                                                </li>
                                                <li>
                                                    <a href="shop-checkout.html">Checkout</a>
                                                </li>
                                                <li>
                                                    <a href="shop-order-received.html">Order Received</a>
                                                </li>

                                            </ul>
                                        </li>
                                        <!-- eof shop -->

                                        <!-- features -->
                                        <li>
                                            <a href="shortcodes_iconbox.html">Shortcodes</a>
                                            <ul>
                                                <li>
                                                    <a href="shortcodes_typography.html">Typography</a>
                                                </li>
                                                <li>
                                                    <a href="shortcodes_buttons.html">Buttons</a>
                                                </li>
                                                <li>
                                                    <a href="shortcodes_pricing.html">Pricing</a>
                                                </li>
                                                <li>
                                                    <a href="shortcodes_iconbox.html">Icon Boxes</a>
                                                </li>
                                                <li>
                                                    <a href="shortcodes_progress.html">Progress</a>
                                                </li>
                                                <li>
                                                    <a href="shortcodes_tabs.html">Tabs &amp; Collapse</a>
                                                </li>
                                                <li>
                                                    <a href="shortcodes_bootstrap.html">Bootstrap Elements</a>
                                                </li>
                                                <li>
                                                    <a href="shortcodes_animation.html">Animation</a>
                                                </li>
                                                <li>
                                                    <a href="shortcodes_icons.html">Template Icons</a>
                                                </li>
                                                <li>
                                                    <a href="shortcodes_socialicons.html">Social Icons</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- eof shortcodes -->

                                        <li>
                                            <a href="shortcodes_widgets_default.html">Widgets</a>
                                            <ul>
                                                <li>
                                                    <a href="shortcodes_widgets_default.html">Default Widgets</a>
                                                </li>
                                                <li>
                                                    <a href="shortcodes_widgets_shop.html">Shop Widgets</a>
                                                </li>
                                                <li>
                                                    <a href="shortcodes_widgets_custom.html">Custom Widgets</a>
                                                </li>
                                            </ul>

                                        </li>


                                        <!-- events -->
                                        <li>
                                            <a href="events-left.html">Events</a>
                                            <ul>
                                                <li>
                                                    <a href="events-left.html">Left Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="events-right.html">Right Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="events-full.html">Full Width</a>
                                                </li>
                                                <li>
                                                    <a href="event-single-left.html">Single Event</a>
                                                    <ul>
                                                        <li>
                                                            <a href="event-single-left.html">Left Sidebar</a>
                                                        </li>
                                                        <li>
                                                            <a href="event-single-right.html">Right Sidebar</a>
                                                        </li>
                                                        <li>
                                                            <a href="event-single-full.html">Full Width</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- eof events -->

                                        <li>
                                            <a href="team.html">Team</a>
                                            <ul>
                                                <li>
                                                    <a href="team.html">Team List</a>
                                                </li>
                                                <li>
                                                    <a href="team-single.html">Team Member</a>
                                                </li>
                                            </ul>
                                        </li>


                                        <li>
                                            <a href="comingsoon.html">Comingsoon</a>
                                        </li>

                                        <li>
                                            <a href="faq.html">FAQ</a>
                                            <ul>
                                                <li>
                                                    <a href="faq.html">FAQ</a>
                                                </li>
                                                <li>
                                                    <a href="faq2.html">FAQ 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="404.html">404</a>
                                        </li>

                                    </ul>
                                </li>
                                <!-- eof pages -->

                                <!-- gallery -->
                                <li>
                                    <a href="gallery-image.html">Gallery</a>
                                    <ul>
                                        <!-- Gallery image only -->
                                        <li>
                                            <a href="gallery-image.html">Image Only</a>
                                            <ul>
                                                <li>
                                                    <a href="gallery-image-2-cols.html">2 columns</a>
                                                </li>
                                                <li>
                                                    <a href="gallery-image.html">3 columns</a>
                                                </li>
                                                <li>
                                                    <a href="gallery-image-4-cols-fullwidth.html">4 columns
                                                        fullwidth</a>
                                                </li>

                                            </ul>
                                        </li>
                                        <!-- eof Gallery image only -->

                                        <!-- Gallery with title -->
                                        <li>
                                            <a href="gallery-title.html">Image With Title</a>
                                            <ul>
                                                <li>
                                                    <a href="gallery-title-2-cols.html">2 columns</a>
                                                </li>
                                                <li>
                                                    <a href="gallery-title.html">3 column</a>
                                                </li>
                                                <li>
                                                    <a href="gallery-title-4-cols-fullwidth.html">4 columns
                                                        fullwidth</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- eof Gallery with title -->

                                        <!-- Gallery with excerpt -->
                                        <li>
                                            <a href="gallery-excerpt.html">Image with Excerpt</a>
                                            <ul>
                                                <li>
                                                    <a href="gallery-excerpt-2-cols.html">2 columns</a>
                                                </li>
                                                <li>
                                                    <a href="gallery-excerpt.html">3 column</a>
                                                </li>
                                                <li>
                                                    <a href="gallery-excerpt-4-cols-fullwidth.html">4 columns
                                                        fullwdith</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- eof Gallery with excerpt -->

                                        <li>
                                            <a href="gallery-tiled.html">Tiled Gallery</a>
                                        </li>

                                        <!-- Gallery item -->
                                        <li>
                                            <a href="gallery-single.html">Gallery Item</a>
                                            <ul>
                                                <li>
                                                    <a href="gallery-single.html">Style 1</a>
                                                </li>
                                                <li>
                                                    <a href="gallery-single2.html">Style 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- eof Gallery item -->
                                    </ul>
                                </li>
                                <!-- eof Gallery -->

                                <!-- contacts -->
                                <li>
                                    <a href="contact.html">Contact</a>
                                    <ul>
                                        <li>
                                            <a href="contact.html">Contact 1</a>
                                        </li>
                                        <li>
                                            <a href="contact2.html">Contact 2</a>
                                        </li>
                                        <li>
                                            <a href="contact3.html">Contact 3</a>
                                        </li>
                                        <li>
                                            <a href="contact4.html">Contact 4</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- eof contacts -->
                            </ul>


                        </nav>
                        <!-- eof main nav -->
                    </div>
                    <div class="col-xl-2 col-lg-4 text-left text-xl-right d-none d-lg-block">

                        <ul class="top-includes">


                            <li>
									<span class="social-icons">

										<a href="https://www.facebook.com/"
                                           class="fa fa-facebook border-icon round color-icon border-icon rounded-icon"
                                           title="facebook"></a>
										<a href="https://twitter.com/"
                                           class="fa fa-twitter border-icon round color-icon border-icon rounded-icon"
                                           title="twitter"></a>
										<a href="https://www.google.com/"
                                           class="fa fa-google border-icon round color-icon border-icon rounded-icon"
                                           title="google"></a>

									</span>
                            </li>


                        </ul>

                    </div>
                </div>
            </div>
            <!-- header toggler -->
            <span class="toggle_menu"><span></span></span>
        </header>

        <section class="page_slider">
            <div class="flexslider" data-dots="true" data-nav="true">
                <ul class="slides">
                    <li class="cs text-center">
                        <img alt="img" src="/front-end/images/slide-bg.jpg">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <div class="intro_layers_wrapper intro_text_bottom">
                                        <div class="intro_layers">
                                            <div class="intro_layer intro-content text-left"
                                                 data-animation="fadeInRight">
                                                <p class="text-uppercase intro_after_featured_word">Best Coworking</p>
                                                <h3 class="text-uppercase intro_before_featured_word">
                                                    qWorx <br> Humanize <br> Work
                                                </h3>
                                                <button type="button" class="btn btn-maincolor">book a tour</button>
                                            </div>
                                            <div class="intro_layer intro-image">
                                                <img alt="img" src="/front-end/images/member1.png">
                                            </div>
                                        </div> <!-- eof .intro_layers -->
                                    </div> <!-- eof .intro_layers_wrapper -->
                                </div> <!-- eof .col-* -->
                            </div><!-- eof .row -->
                        </div><!-- eof .container-fluid -->
                    </li>
                    <li class="cs text-center">
                        <img alt="img" src="/front-end/images/slide-bg2.jpg">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="intro_layers_wrapper intro_text_bottom">
                                        <div class="intro_layers">
                                            <div class="intro_layer intro-content text-left"
                                                 data-animation="fadeInRight">
                                                <p class="text-uppercase intro_after_featured_word">Best Coworking</p>
                                                <h3 class="text-uppercase intro_before_featured_word">
                                                    qWorx<br> Humanize<br> Work
                                                </h3>
                                                <button type="button" class="btn btn-maincolor2">book a tour</button>
                                            </div>
                                            <div class="intro_layer intro-image">
                                                <img alt="img" src="/front-end/images/member2.png">
                                            </div>
                                        </div> <!-- eof .intro_layers -->
                                    </div> <!-- eof .intro_layers_wrapper -->
                                </div> <!-- eof .col-* -->
                            </div><!-- eof .row -->
                        </div><!-- eof .container-fluid -->

                    <li class="cs text-center">
                        <img alt="img" src="/front-end/images/slide-bg3.jpg">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="intro_layers_wrapper intro_text_bottom">
                                        <div class="intro_layers">
                                            <div class="intro_layer intro-content text-left"
                                                 data-animation="fadeInRight">
                                                <p class="text-uppercase intro_after_featured_word">Best Coworking</p>
                                                <h3 class="text-uppercase intro_before_featured_word">
                                                    qWorx<br> Humanize<br> Work
                                                </h3>
                                                <button type="button" class="btn btn-maincolor3">book a tour</button>
                                            </div>
                                            <div class="intro_layer intro-image">
                                                <img alt="img" src="/front-end/images/member3.png">
                                            </div>
                                        </div> <!-- eof .intro_layers -->
                                    </div> <!-- eof .intro_layers_wrapper -->
                                </div> <!-- eof .col-* -->
                            </div><!-- eof .row -->
                        </div><!-- eof .container-fluid -->
                    </li>
                </ul>
            </div> <!-- eof flexslider -->
        </section>
        <section class="s-pt-50 s-pb-50 s-pt-md-90 s-pb-md-90 s-pt-lg-50 s-pb-lg-120 s-pb-xl-100 s-pt-xl-80 ls">
            <div class="container">
                <div class="row">
                    <div class="divider-70 d-none d-lg-block"></div>
                    <div class="col-12">
                        <p class="feature-top text-center">Coupled with high speed internet, open and spacious
                            workspaces, we
                            provide an amazing place for our members.</p>
                        <div class="owl-carousel features-carousel" data-margin="18" data-responsive-lg="3"
                             data-responsive-md="2" data-responsive-sm="2" data-responsive-xs="1" data-dots="true"
                             data-nav="true" data-loop="true">
                            <div class="features-item text-center">
                                <div class="features__img round">
                                    <a href="#">
                                        <div class="overlay"><span>read more</span></div>
                                        <img src="/front-end/images/feature2.jpg" height="1281" width="1920" alt="img"/>
                                    </a>
                                </div>
                                <div class="features__content">
                                    <h4 class="features__title color-main3"><a href="#">Co-Working Areas</a></h4>
                                    <p class="features__text">
                                        Co-working areas inspires to creativity and help to build your network.
                                    </p>
                                </div>
                            </div>
                            <div class="features-item text-center">
                                <div class="features__img round">
                                    <a href="#">
                                        <div class="overlay"><span>read more</span></div>
                                        <img src="/front-end/images/feature1.jpg" height="1281" width="1920" alt="img"/>
                                    </a>
                                </div>
                                <div class="features__content">
                                    <h4 class="features__title  color-main2"><a href="#">Business Essentials</a></h4>
                                    <p class="features__text">
                                        We offer business address, mailbox service, lockers and access to printing.
                                    </p>
                                </div>
                            </div>
                            <div class="features-item text-center">
                                <div class="features__img round">
                                    <a href="#">
                                        <div class="overlay"><span>read more</span></div>
                                        <img src="/front-end/images/feature3.jpg" height="1281" width="1920" alt="img"/>
                                    </a>
                                </div>
                                <div class="features__content">
                                    <h4 class="features__title  color-main"><a href="#">Dedicated Desks</a></h4>
                                    <p class="features__text">
                                        Our unique, beautifully designed private spaces are available.
                                    </p>
                                </div>
                            </div>
                            <div class="features-item">
                                <div class="features__img round">
                                    <a href="#">
                                        <div class="overlay"><span>read more</span></div>
                                        <img src="/front-end/images/feature2.jpg" height="1281" width="1920" alt="img"/>
                                    </a>
                                </div>
                                <div class="features__content text-center">
                                    <h4 class="features__title color-main3"><a href="#">Co-Working Areas</a></h4>
                                    <p class="features__text">
                                        Co-working areas inspires to creativity and help to build your network.
                                    </p>
                                </div>
                            </div>
                            <div class="features-item">
                                <div class="features__img round">
                                    <a href="#">
                                        <div class="overlay"><span>read more</span></div>
                                        <img src="/front-end/images/feature3.jpg" height="1281" width="1920" alt="img"/>
                                    </a>
                                </div>
                                <div class="features__content text-center">
                                    <h4 class="features__title color-main"><a href="#">Co-Working Areas</a></h4>
                                    <p class="features__text">
                                        Co-working areas inspires to creativity and help to build your network.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider-150 d-none d-xl-block"></div>
                    <div class="divider-0 d-none d-lg-block d-xl-none"></div>
                </div>
            </div>
        </section>
        <section
            class="events s-pt-60 s-pb-50 s-pt-md-100 s-pb-md-90 s-pt-lg-130 s-pb-lg-120 s-py-xl-70 overflow-visible ls ms">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="events__title text-center special-heading">
                            <p>UPCOMING</p>
                            <h2>EVENTS</h2>
                        </div>
                        <div class="events">
                            <div class="events__item rounded light-bg">
                                <div class="events__content">
                                    <div class="events__date">
                                        <div class="date__left color-main">
                                            <span class="day">24</span>
                                        </div>
                                        <div class="date__right">
                                            <span class="month">Feb</span>
                                            <span class="year">sunday</span>
                                        </div>
                                    </div>
                                    <div class="events__media">
                                        <img src="/front-end/images/square/03.jpg" height="222" width="370" alt="img"/>
                                    </div>
                                    <h5 class="events__meta">Escape Winter Destination Cowork on Beach</h5>
                                    <div class="events__link">
                                        <a href="#" class="btn btn-maincolor">Attend</a>
                                    </div>
                                </div>
                            </div>
                            <div class="events__item rounded light-bg">
                                <div class="events__content">
                                    <div class="events__date">
                                        <div class="date__left color-main2">
                                            <span class="day">23</span>
                                        </div>
                                        <div class="date__right">
                                            <span class="month">Feb</span>
                                            <span class="year">saturday</span>
                                        </div>
                                    </div>
                                    <div class="events__media">
                                        <img src="/front-end/images/square/05.jpg" height="222" width="370" alt="img"/>
                                    </div>
                                    <h5 class="events__meta">Effective Talent Acquisition Strategies by Melissa
                                        Core</h5>
                                    <div class="events__link">
                                        <a href="#" class="btn btn-maincolor2">Attend</a>
                                    </div>
                                </div>
                            </div>
                            <div class="events__item rounded light-bg">
                                <div class="events__content">
                                    <div class="events__date">
                                        <div class="date__left color-main3">
                                            <span class="day">22</span>
                                        </div>
                                        <div class="date__right">
                                            <span class="month">Feb</span>
                                            <span class="year">friday</span>
                                        </div>
                                    </div>
                                    <div class="events__media">
                                        <img src="/front-end/images/square/07.jpg" height="222" width="370" alt="img"/>
                                    </div>
                                    <h5 class="events__meta">Connect Panel Discussion & Enrerpreneur Happy Hour</h5>
                                    <div class="events__link">
                                        <a href="#" class="btn btn-maincolor3">Attend</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="events__more text-center">all events</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="s-pt-60 s-pb-30 s-pt-md-100 s-pb-md-50 s-pt-lg-10 s-pb-lg-60 s-pt-xl-90 ls">
            <div class="container">

                <div class="row">

                    <div class="divider-150 d-none d-lg-block"></div>

                    <div class="col-xs-12 col-lg-4">

                        <div class="pricing-plan box-shadow">
                            <div class="plan-name">
                                <h3>
                                    FLOATING DESK
                                </h3>
                            </div>
                            <div class="price-wrap color-darkgrey">
                                <span class="plan-price">250</span>
                            </div>
                            <div class="plan-description small-text">
                                PER MONTH
                            </div>
                            <div class="plan-features">
                                <ul class="list-bordered">
                                    <li>Up to 5 Team Members</li>
                                    <li>1 Project</li>
                                    <li>Invoicing & Expence Tracking</li>
                                    <li>Q+A Call</li>
                                    <li>Email & Chat Support</li>
                                </ul>
                            </div>
                            <div class="plan-button">
                                <a href="#" class="btn btn-maincolor">Purchase</a>
                            </div>
                        </div>

                    </div>


                    <div class="col-xs-12 col-lg-4">

                        <div class="pricing-plan plan-featured box-shadow">
                            <div class="plan-name">
                                <h3>
                                    Business Plan
                                </h3>
                            </div>
                            <div class="offer1">
                                <h6>BEST OFFER</h6>
                            </div>
                            <div class="price-wrap color-darkgrey">
                                <span class="plan-price">350</span>
                            </div>
                            <div class="plan-description small-text">
                                PER MONTH
                            </div>
                            <div class="plan-features">
                                <ul class="list-bordered">
                                    <li>Up to 10 Team Members</li>
                                    <li>5 Project</li>
                                    <li>Invoicing & Expence Tracking</li>
                                    <li>Written Report</li>
                                    <li>Email & Chat Support</li>
                                </ul>
                            </div>
                            <div class="plan-button">
                                <a href="#" class="btn btn-maincolor2">Purchase</a>
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-lg-4 mx-sm-auto">

                        <div class="pricing-plan third box-shadow">
                            <div class="plan-name">
                                <h3>
                                    Expert Plan
                                </h3>
                            </div>
                            <div class="price-wrap color-darkgrey">
                                <span class="plan-price">450</span>
                            </div>
                            <div class="plan-description small-text">
                                PER MONTH
                            </div>
                            <div class="plan-features">
                                <ul class="list-bordered">
                                    <li>Up to 30 Team Members</li>
                                    <li>10 Project</li>
                                    <li>Invoicing & Expence Tracking</li>
                                    <li>Written Report</li>
                                    <li>Email & Chat Support</li>
                                </ul>
                            </div>
                            <div class="plan-button">
                                <a href="#" class="btn btn-maincolor3">Purchase</a>
                            </div>
                        </div>

                    </div>

                    <div class="divider-90 d-none d-lg-block"></div>
                </div>


            </div>

        </section>

        <section
            class="overflow-visible s-free s-pt-60 s-pb-70 s-pt-md-100 s-pb-md-140 s-pt-lg-130 s-pb-lg-30 s-pt-xl-160 s-parallax ds">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="free-title text-center special-heading">
                            <p>Download Free</p>
                            <h2>7 Day Pass</h2>
                            <p class="title-discription">
                                Explore what we can offer!
                            </p>
                        </div>
                        <form class="text-center" action="#">
                            <div class="free-input">
                                <input class="rounded" type="text" placeholder="E-mail" required>
                                <input class="rounded" type="tel" placeholder="Phone" required>
                                <select class="rounded" name="select" required>
                                    <option value="" selected>Business select</option>
                                    <option value="Business select">Business select</option>
                                    <option value="Business select">Business select</option>
                                    <option value="Business select">Business select</option>
                                </select>
                            </div>
                            <div class="free-button">
                                <div class="free-chek">
                                    <input type="checkbox" id="scales" name="feature" value="scales" checked/>
                                    <label for="scales">Notify me about upcoming events</label>
                                </div>
                                <button class="btn btn-maincolor4" type="submit">Get my pass</button>
                            </div>

                        </form>
                    </div>
                    <div class="divider-260 d-none d-lg-block"></div>
                </div>
            </div>
        </section>

        <section class="c-gutter-0 s-pb-40 ls overflow-visible">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="location__item">
                            <div class="location__title">
                                <h5>SAN DIEGO</h5>
                            </div>
                            <div class="location__disc">
                                <p class="text-center">1600 Byers Lane, San Diego, CA 92101</p>
                            </div>
                            <div class="location__tel">
                                <span>1-800-123-4567</span>
                            </div>
                            <button class="btn btn btn-outline-maincolor">learn more</button>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="location__item loc2">
                            <div class="location__title">
                                <h5>Miami</h5>
                            </div>
                            <div class="location__disc">
                                <p class="text-center">1023 Arbutus Drive, Miami, FL 33131</p>
                            </div>
                            <div class="location__tel">
                                <span>1-800-456-7890</span>
                            </div>
                            <button class="btn btn btn-outline-maincolor3">learn more</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section_testimonials"
                 class="s-pt-xl-100 s-pb-xl-90 s-pt-lg-70 s-pb-lg-60 s-pb-md-90 s-pt-md-40 s-pt-0 s-pb-50 ls">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <div class="testimonials-slider owl-carousel" data-autoplay="false" data-loop="true"
                             data-responsive-lg="1" data-responsive-md="1" data-responsive-sm="1" data-nav="false"
                             data-dots="true">
                            <div class="quote-item">
                                <div class="quote-image">
                                    <img src="/front-end/images/square/01.jpg" alt="img">
                                </div>
                                <blockquote>
                                    <footer>
                                        STEVEN BAUER
                                        <div class="star-rating">
                                            <span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span>
                                        </div>
                                    </footer>

                                    <p>
                                        Right after moving in <strong>QWorx</strong> became a huge resource for us in
                                        our growth efforts. We found that the type of companies here were all in growth
                                        mode which creates a really fun energy and a place.
                                    </p>

                                </blockquote>
                            </div>


                            <div class="quote-item">
                                <div class="quote-image">
                                    <img src="/front-end/images/square/02.jpg" alt="img">
                                </div>
                                <blockquote>
                                    <footer>
                                        STEVEN BAUER
                                        <div class="star-rating">
                                            <span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span>
                                        </div>
                                    </footer>

                                    <p>
                                        Right after moving in <strong>QWorx</strong> became a huge resource for us in
                                        our growth efforts. We found that the type of companies here were all in growth
                                        mode which creates a really fun energy and a place.
                                    </p>

                                </blockquote>
                            </div>


                            <div class="quote-item">
                                <div class="quote-image">
                                    <img src="/front-end/images/square/04.jpg" alt="img">
                                </div>
                                <blockquote>
                                    <footer>
                                        STEVEN BAUER
                                        <div class="star-rating text-center">
                                            <span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span>
                                        </div>
                                    </footer>

                                    <p>
                                        Right after moving in <strong>QWorx</strong> became a huge resource for us in
                                        our growth efforts. We found that the type of companies here were all in growth
                                        mode which creates a really fun energy and a place.
                                    </p>

                                </blockquote>
                            </div>

                        </div><!-- .testimonials-slider -->

                    </div>
                    <div class="divider-150 d-none d-xl-block"></div>
                    <div class="divider-60 d-none d-lg-block d-xl-none"></div>
                </div>
            </div>
        </section>
        <section
            class="s-pt-xl-130 s-pb-xl-100 s-pt-lg-130 s-pb-lg-70 s-pt-md-160 s-pb-md-40 s-pt-120 c-mb-50 s-steps overflow-visible ls ms">
            <div class="container">
                <div class="row">
                    <div class="divider-60 d-none d-lg-block"></div>
                    <div class="col-md-12 col-lg-4">
                        <div class="steps rounded light-bg">
                            <div class="steps__media round bg-maincolor">
                                <h2>01</h2>
                            </div>
                            <div class="steps__title">
                                <h5>JOIN US</h5>
                            </div>
                            <div class="steps__meta text-center">
                                <p>Our all-inclusive memberships lets you to choose what works for you.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4">
                        <div class="steps rounded light-bg">
                            <div class="steps__media round bg-maincolor2">
                                <h2>02</h2>
                            </div>
                            <div class="steps__title">
                                <h5>FIND YOUR SPOT</h5>
                            </div>
                            <div class="steps__meta text-center">
                                <p>Whether need dedicated desks or private offices, we provide the space you need.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4">
                        <div class="steps rounded light-bg mb-0">
                            <div class="steps__media round bg-maincolor3">
                                <h2>03</h2>
                            </div>
                            <div class="steps__title">
                                <h5>WORK & CONNECT</h5>
                            </div>
                            <div class="steps__meta text-center ">
                                <p>Claim your space among people with businesses who are ready to connect.</p>
                            </div>
                        </div>
                        <div class="divider-10"></div>
                    </div>
                </div>
            </div>
        </section>

        <section class="ds c-gutter-0 container-px-0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="owl-carousel small-gallery-carousel" data-margin="0" data-responsive-lg="8"
                             data-responsive-md="4" data-responsive-sm="2" data-responsive-xs="1" data-nav="true"
                             data-loop="false">
                            <a href="images/square/04.jpg" class="photoswipe-link" data-width="800" data-height="800">
                                <img src="/front-end/images/square/04.jpg" alt="img">
                            </a>
                            <a href="images/square/05.jpg" class="photoswipe-link" data-width="800" data-height="800"
                               data-iframe="https://www.youtube.com/embed/mcixldqDIEQ">
                                <img src="/front-end/images/square/05.jpg" alt="img">
                            </a>
                            <a href="images/square/06.jpg" class="photoswipe-link" data-width="800" data-height="800"
                               data-iframe="//player.vimeo.com/video/1084537">
                                <img src="/front-end/images/square/06.jpg" alt="img">
                            </a>
                            <a href="images/square/07.jpg" class="photoswipe-link" data-width="800" data-height="800">
                                <img src="/front-end/images/square/07.jpg" alt="img">
                            </a>
                            <a href="images/square/08.jpg" class="photoswipe-link" data-width="800" data-height="800">
                                <img src="/front-end/images/square/08.jpg" alt="img">
                            </a>
                            <a href="images/square/09.jpg" class="photoswipe-link" data-width="800" data-height="800">
                                <img src="/front-end/images/square/09.jpg" alt="img">
                            </a>
                            <a href="images/square/10.jpg" class="photoswipe-link" data-width="800" data-height="800">
                                <img src="/front-end/images/square/10.jpg" alt="img">
                            </a>
                            <a href="images/square/11.jpg" class="photoswipe-link" data-width="800" data-height="800">
                                <img src="/front-end/images/square/11.jpg" alt="img">
                            </a>
                            <a href="images/square/01.jpg" class="photoswipe-link" data-width="800" data-height="800">
                                <img src="/front-end/images/square/01.jpg" alt="img">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="ls ms page_map" data-draggable="false" data-scrollwheel="false">

            <div class="marker">
                <div class="marker-address">sydney, australia, Liverpool street, 66</div>
                <div class="marker-title">Reach Us</div>
                <div class="marker-description">

                    <ul class="list-unstyled">
                        <li>
								<span class="icon-inline">
									<span class="icon-styled color-main">
										<i class="fa fa-map-marker"></i>
									</span>

									<span>
										Sydney, Australia, Liverpool street, 66
									</span>
								</span>
                        </li>

                        <li>
								<span class="icon-inline">
									<span class="icon-styled color-main">
										<i class="fa fa-phone"></i>
									</span>

									<span>
										1 (800) 123-45-67
									</span>
								</span>
                        </li>
                        <li>
								<span class="icon-inline">
									<span class="icon-styled color-main">
										<i class="fa fa-envelope"></i>
									</span>

									<span>
										mail@example.com
									</span>
								</span>
                        </li>
                    </ul>
                </div>

                <img class="marker-icon" src="/front-end/images/map_marker_icon.png" alt="img">
            </div>
            <!-- .marker -->

        </section>
        <div class="fw-divider-space mt--60"></div>
        <footer class="page_footer s-parallax ds s-pt-100 s-pt-md-180 s-pb-55 s-pb-lg-163 s-pb-xl-163 c-gutter-60">
            <div class="container">
                <div class="row">
                    <div class="divider-20 d-none d-xl-block"></div>
                    <div class="col-md-12 text-center animate" data-animation="fadeInUp">
                        <div class="logo_footer">
                            <a href="#">
                                <img src="/front-end/images/logo-footer.png" alt="img">
                            </a>
                        </div>
                        <div class="widget widget_icons_list">
                            <ul>
                                <li class="icon-inline my-10">
                                    <div class="icon-styled icon-top color-main3 fs-14">
                                        <i class="fa fa-pencil"></i>
                                    </div>
                                    <p>
                                        <a href="#">email@example.com</a>
                                    </p>
                                </li>
                                <li class="icon-inline my-10">
                                    <div class="icon-styled icon-top color-main2 fs-14">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <p>90000, New Str. 123, Los Angeles, CA</p>
                                </li>
                                <li class="icon-inline my-10">
                                    <div class="icon-styled icon-top color-main fs-14">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <p>1-800-123-4567</p>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <div class="col-md-12 text-center animate" data-animation="fadeInUp">
                        <div class="widget widget_mailchimp">


                            <form class="signup" action="http://webdesign-finder.com/">

                                <input name="email" type="email" class="mailchimp_email" placeholder="Email Newsletter">

                                <button type="submit" class="search-submit">
                                    <span class="screen-reader-text">Subscribe</span>
                                </button>
                                <div class="response"></div>
                            </form>

                        </div>
                    </div>


                    <div class="col-md-12 mt-2 text-center animate" data-animation="fadeInUp">
							<span class="social-icons">

								<a href="#" class="fa fa-facebook border-icon round color-icon border-icon rounded-icon"
                                   title="facebook"></a>
								<a href="#" class="fa fa-twitter border-icon round color-icon border-icon rounded-icon"
                                   title="twitter"></a>
								<a href="#" class="fa fa-google border-icon round color-icon border-icon rounded-icon"
                                   title="google"></a>

							</span>
                    </div>

                </div>
            </div>
        </footer>

        <section class="page_copyright ls s-py-5">
            <div class="container">
                <div class="row align-items-center">
                    <div class="divider-20 d-none d-lg-block"></div>
                    <div class="col-md-12 text-center">
                        <p>&copy; Copyright <span class="copyright_year">2018</span> All Rights Reserved</p>
                    </div>
                    <div class="divider-20 d-none d-lg-block"></div>
                </div>
            </div>
        </section>


    </div><!-- eof #box_wrapper -->
</div><!-- eof #canvas -->


<script src="/front-end/js/compressed.js"></script>
<script src="/front-end/js/main.js"></script>
<script src="/front-end/js/switcher.js"></script>

<!-- Google Map Script -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap"></script>

</body>


<!-- Mirrored from webdesign-finder.com/html/qworks/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 Jan 2020 15:33:26 GMT -->
</html>
